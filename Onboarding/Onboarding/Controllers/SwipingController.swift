//
//  SwipingController.swift
//  Onboarding
//
//  Created by Jansen Ducusin on 4/6/21.
//

import UIKit

class SwipingController: UICollectionViewController {
    // MARK: - Properties
    
    private let previousButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("PREV", for: .normal)
        button.setTitleColor(.gray, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(previousButtonHandler), for: .touchUpInside)
        return button
    }()
    
    private let nextButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle("NEXT", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitleColor(.themePink, for: .normal)
        button.addTarget(self, action: #selector(nextButtonHandler), for: .touchUpInside)
        return button
    }()
    
    private lazy var pageControl: UIPageControl = {
        let pageControl = UIPageControl()
        pageControl.currentPage = 0
        pageControl.numberOfPages = PageViewModel.allCases.count
        pageControl.currentPageIndicatorTintColor = .systemRed
        pageControl.pageIndicatorTintColor = .themeLightPink
        pageControl.isUserInteractionEnabled = false
        return pageControl
    }()
    
    // MARK: - Lifecycles
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(PageCell.self, forCellWithReuseIdentifier: PageCell.cellIdentifier)
        collectionView.isPagingEnabled = true
        collectionView.backgroundColor = .white
        self.setupNavigationControls()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { [weak self] _ in
            
            /* Alternatively, invalidateLayout() can be used instead of reloadData() but causes the first page to clear in some devices/simulator?.
             
                self.collectionViewLayout.invalidateLayout()
             
             */
            
            guard let pageIndex = self?.pageControl.currentPage else {
                return
            }
            
            self?.collectionView.reloadData()
            self?.movePage(withIndex: pageIndex)
            
        }, completion: nil)
        
    }

    // MARK: - Selectors
    @objc private func nextButtonHandler(){
        guard pageControl.currentPage < 3 else {
            return
        }
        
        let nextIndex = pageControl.currentPage + 1
        self.movePage(withIndex: nextIndex)
    }
    
    @objc private func previousButtonHandler(){
        guard pageControl.currentPage > 0 else {
            return
        }
        let previousIndex = pageControl.currentPage - 1
        
        self.movePage(withIndex: previousIndex)
    }
    
    // MARK: - Helpers
    private func movePage(withIndex index:Int){
        let indexPath = IndexPath(item: index, section: 0)
        self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        pageControl.currentPage = index
    }
    
    private func setupNavigationControls(){
        let stack = UIStackView(arrangedSubviews: [self.previousButton, self.pageControl, self.nextButton])
        stack.translatesAutoresizingMaskIntoConstraints = false
        
        self.previousButton.widthAnchor.constraint(equalToConstant: 85).isActive = true
        self.nextButton.widthAnchor.constraint(equalToConstant: 85).isActive = true
        
        self.view.addSubview(stack)
        
        stack.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 5).isActive = true
        stack.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        stack.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
    }
    
}

// MARK: - CollectionView Datasource and Delegate
extension SwipingController: UICollectionViewDelegateFlowLayout {
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return PageViewModel.allCases.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PageCell.cellIdentifier, for: indexPath) as! PageCell
        
        cell.viewModel = PageViewModel(rawValue: indexPath.row)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width, height: self.view.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}

// MARK: - ScrollView Delegate
extension SwipingController {
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let x = targetContentOffset.pointee.x
        pageControl.currentPage = Int(x/view.frame.width)
    }
}
