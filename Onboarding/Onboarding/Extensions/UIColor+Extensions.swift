//
//  UIColor+Extensions.swift
//  Onboarding
//
//  Created by Jansen Ducusin on 4/6/21.
//

import UIKit

extension UIColor {
    static let themePink = UIColor(red: 232/255, green: 68/255, blue: 133/255, alpha: 1)
    static let themeLightPink = UIColor(red: 249/255, green: 207/255, blue: 224/255, alpha: 1)
}
