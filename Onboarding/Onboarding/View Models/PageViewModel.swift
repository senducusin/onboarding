//
//  PageViewModel.swift
//  Onboarding
//
//  Created by Jansen Ducusin on 4/6/21.
//

import UIKit

enum PageViewModel: Int, CaseIterable{
    case page0, page1, page2, page3
    
    var description: NSAttributedString {
        switch self {
        case .page0:
            return attributedText(header: "What is Lorem Ipsum?", body: "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.")
        case .page1:
            return attributedText(header: "Where does it come from?", body: "It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.")
        case .page2:
            return attributedText(header: "Why do we use it?", body: "It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.")
        case .page3:
            return attributedText(header: "Where can I get some?", body: "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.")
        }
    }
    
    var image: UIImage? {
        switch self{
        case .page0:
            return UIImage(systemName: "book")
        case .page1:
            return UIImage(systemName: "character.book.closed")
        case .page2:
            return UIImage(systemName: "bookmark")
        case .page3:
            return UIImage(systemName: "books.vertical")
        }
    }
    
    var alignment: NSTextAlignment {
        return .center
    }
    
}

extension PageViewModel {
    private func attributedText(header:String, body:String) -> NSAttributedString {
        let attributedText = NSMutableAttributedString(string: header, attributes: [NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: 18)])

        attributedText.append(NSAttributedString(string: "\n\n\n\(body)", attributes: [
            NSAttributedString.Key.font: UIFont.systemFont(ofSize: 13),
            NSAttributedString.Key.foregroundColor:UIColor.gray
        ]))
        
        return attributedText
    }
}
