//
//  PageCell.swift
//  Onboarding
//
//  Created by Jansen Ducusin on 4/6/21.
//

import UIKit

class PageCell: UICollectionViewCell {
    // MARK: - Properties
    var viewModel: PageViewModel? {
        didSet{
            self.configure()
        }
    }
    
    static let cellIdentifier = "PageCell"
    
    private let topContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let bottomContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let topImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.tintColor = .systemTeal
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private let descriptionTextView: UITextView = {
        let textView = UITextView()
        
        textView.isEditable = false
        textView.isScrollEnabled = false
        
        return textView
    }()
    
    // MARK: - Lifecycles
    override init(frame: CGRect){
        super.init(frame: frame)
        
        self.setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Helpers
    private func configure(){
        guard let vm = viewModel else {
            return
        }
        
        self.topImageView.image = vm.image
        self.descriptionTextView.attributedText = vm.description
        self.descriptionTextView.textAlignment = vm.alignment
    }
    
    private func setupUI(){
        self.backgroundColor = .white
        
        self.setupTopViewContainer()
        self.setupBottomViewContainer()
    }
    
    private func setupTopViewContainer(){
        self.addSubview(self.topContainerView)
        self.topContainerView.heightAnchor.constraint(equalTo: self.contentView.heightAnchor, multiplier: 0.5).isActive = true
        self.topContainerView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        self.topContainerView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        self.topContainerView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        
        self.setupBookImageView()
    }
    
    private func setupBottomViewContainer(){
        self.addSubview(self.bottomContainerView)
        self.bottomContainerView.translatesAutoresizingMaskIntoConstraints = false
        self.bottomContainerView.heightAnchor.constraint(equalTo: self.contentView.heightAnchor, multiplier: 0.5).isActive = true
        
        self.bottomContainerView.topAnchor.constraint(equalTo: self.topContainerView.bottomAnchor).isActive = true
        self.bottomContainerView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 24).isActive = true
        self.bottomContainerView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -24).isActive = true
        self.bottomContainerView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        self.setupTextView()
    }
    
    private func setupTextView(){
        self.addSubview(self.descriptionTextView)
        self.descriptionTextView.translatesAutoresizingMaskIntoConstraints = false
        self.descriptionTextView.topAnchor.constraint(equalTo: self.bottomContainerView.topAnchor).isActive = true
        self.descriptionTextView.leftAnchor.constraint(equalTo: self.bottomContainerView.leftAnchor).isActive = true
        self.descriptionTextView.rightAnchor.constraint(equalTo: self.bottomContainerView.rightAnchor).isActive = true
        self.descriptionTextView.bottomAnchor.constraint(equalTo: self.bottomContainerView.bottomAnchor).isActive = true
    }
 
    private func setupBookImageView(){
        self.topContainerView.addSubview(self.topImageView)
        self.topImageView.translatesAutoresizingMaskIntoConstraints = false
        self.topImageView.centerXAnchor.constraint(equalTo: topContainerView.centerXAnchor).isActive = true
        self.topImageView.centerYAnchor.constraint(equalTo: topContainerView.centerYAnchor).isActive = true
        
        self.topImageView.heightAnchor.constraint(equalTo: self.topContainerView.heightAnchor, multiplier: 0.5).isActive = true
        self.topImageView.widthAnchor.constraint(equalTo: self.topContainerView.heightAnchor, multiplier: 0.5).isActive = true
    }
    
}
