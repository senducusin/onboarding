**Onboarding**

A sample app that demonstrates the use of UICollectionView in creating an onboarding slider.

This app does not use storyboards except for the launch screen.  

---

## Features

- It has a 'Next' and 'Previous' buttons to navigate the onboarding view
- Swipe gestures are supported to navigate the onboarding view
- Uses a page control to show the current page

---

## Demo

![](/Previews/Preview.gif)